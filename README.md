# Linux

## SSH

Stiahneme si [nastav-ssh-kluc](nastav-ssh-kluc):

```
wget https://gitlab.com/petermannsk/linux/-/raw/main/nastav-ssh-kluc
```
Samozrejme si skontrolujeme, čo spúšťame:
```
cat nastav-ssh-kluc
```
A ak je to v poriadku, tak to spustíme:
```
chmod +x nastav-ssh-kluc

./nastav-ssh-kluc
```
Skontrolujeme výsledok skriptu:
```
cat ~/.ssh/authorized_keys
```
A hotovo.

